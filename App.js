import React, { useState } from 'react';
import Box from './components/Box';
import Buttons from './components/Buttons';

const App = () => {
  const [count,setCount] = useState(0);
  const [name,setname] = useState('Brandon')

  const handleIncrement = () =>{
    setCount(count+1);
  }

  const handleDerecremnt = () =>{
    setCount(count-1);
  }

  const handleReset = () =>{
    setCount(0);
  }

  const handleMultiply = () =>{
    setCount(count*2);
  }

  return ( 
    <div 
      className="d-flex flex-column justify-content-center align-items-center my-5">
      <Box 
        count={count}
        name={name}
      />
      <Buttons 
        handleIncrement = {handleIncrement}
        handleDecrement = {handleDerecremnt}
        handleReset = {handleReset}
        handleMultiply = {handleMultiply}
      />
    </div>
  );
}
 
export default App;

