import React from 'react';

const Box = ({count,name}) => {
    return ( 
			<div 
				style={{
					width: "300px", 
					height:"300px",
					border: "5px solid black"
				}}
				className="d-flex justify-content-center align-items-center flex-column"
			>
				<h1
					style={{fontSize: '60px'}}
				>{name}:</h1>
				<h1
					style={{fontSize: '120px'}}
				>{count}</h1>
			</div>
    );
}
 
export default Box;