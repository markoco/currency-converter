import React from 'react';

const Button = ({color,text,task}) => {
    return ( 
        <button 
						className={color}
						onClick={task}
				>{text}
        </button>
     );
}
 
export default Button;