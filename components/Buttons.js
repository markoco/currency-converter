import React from 'react';
import Button from './button/Button'
const Buttons = ({handleIncrement,handleDecrement,handleReset,handleMultiply}) => {
    return ( 
        <div 
					className="d-flex justify-content-around w-50"
					style={{
						margin: "50px 100px"
					}}
        >
						<Button 
							color={'btn btn-danger'}
							text={'+1'}
							task={handleIncrement}
						/>
						<Button 
							color={'btn btn-success'}
							text={'-1'}
							task={handleDecrement}
						/>
						<Button 
							color={'btn btn-info'}
							text={'Reset'}
							task={handleReset}
						/>
						<Button 
							color={'btn btn-warning'}
							text={'x2'}
							task={handleMultiply}
						/>
        </div>
    );
}
 
export default Buttons;