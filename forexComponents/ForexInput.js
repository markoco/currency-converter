import React from 'react';
import { Input,FormGroup,Label } from 'reactstrap';

const ForexInput = ({label,placeholder,defaultValue,onChange}) => {
    return ( 
        <FormGroup>
            <Label>{label}</Label>
                <Input
                    placeholder ={placeholder}
                    defaultValue ={defaultValue}
                    onChange = {onChange}
                    type = 'number'
                />
        </FormGroup>
     );
}
 
export default ForexInput;