import React, { useState } from 'react';
import Btn from './Btn';
const Calculator2 = () => {
    const [numbe,setNumber] = useState('')
    const handleNumbers = number => {
        setNumber(numbe+number);
        console.log(numbe);
        
        
    }

    return ( 
        <div className="col-lg-3 bg-dark py-2">
            <div className="bg-light " style={{height: "75px", width: "100%"}}></div>
            <Btn text = "AC"/>
            <Btn text = "+/-"/>
            <Btn text = "%"/>
            <Btn text = "÷"/>
            <Btn text = "7" handleNumbers={handleNumbers}/>
            <Btn text = "8" handleNumbers={handleNumbers}/>
            <Btn text = "9" handleNumbers={handleNumbers}/>
            <Btn text = "x"/>
            <Btn text = "4" handleNumbers={handleNumbers}/>
            <Btn text = "5" handleNumbers={handleNumbers}/>
            <Btn text = "6" handleNumbers={handleNumbers}/>
            <Btn text = "-"/>
            <Btn text = "1" handleNumbers={handleNumbers}/>
            <Btn text = "2" handleNumbers={handleNumbers}/>
            <Btn text = "3" handleNumbers={handleNumbers}/>
            <Btn text = "+" handleNumbers={handleNumbers}/>
            <Btn text = "0"/>
            <Btn text = "-"/>
            <Btn text = "="/>
        </div>
        
     );
}
 
export default Calculator2;