import React, { useState } from 'react';
import ForexDropdown from './ForexDropdown'
import ForexInput from './ForexInput'
import {Button} from 'reactstrap';
import Rates from './Rates';

const Forex = () =>{


    const [amount,setAmount] = useState(0);
    const [baseCurrency, setBaseCurrency] = useState(null);
    const [targetCurrency, setTargetCurrency] = useState(null);
    const [convertedAmount, setConvertedAmount] = useState(0);
    const [rates, setRates] = useState({});

    const handleAmount = e =>{
       setAmount(e.target.value);
    }

    const handleConvert = () =>{
        const code = baseCurrency.code;
        
        fetch('https://api.exchangeratesapi.io/latest?base=' + code)
        .then(res => res.json())
        .then( res => {
            console.log(Object.keys(res.rates));
            const targetCode = targetCurrency.code;
            const rate = res.rates[targetCode];
            setConvertedAmount(amount * rate + " " + targetCode)
            
        });
    }

    const handleBaseCurrency = currency => {
        setBaseCurrency(currency);

        const code = currency.code
        
        fetch('https://api.exchangeratesapi.io/latest?base=' + code)
        .then(res => res.json())
        .then( res => {
            setRates(res.rates);
        });
    }

    const handleTargetCurrency = currency => {
        setTargetCurrency(currency);  
    }

    return ( 
        <div style={{width: "70%", backgroundColor: "blue"}}>
            <h1 className='text-center my-5'>Forex Calculator</h1>
            <div className="d-flex justify-content-around"
                style={{margin:'0 200px'}}
            >
                <ForexDropdown
                    label ={'Base Currrency'}
                    onClick = {handleBaseCurrency}
                    currency = {baseCurrency}
                    />
                    <ForexDropdown
                    label ={'Target Currrency'}
                    onClick = {handleTargetCurrency}
                    currency = {targetCurrency}
                    />
            </div>
            <div className="d-flex justify-content-around">
                <ForexInput
                    label = {'Amount'}
                    placeholder ={'Amout to Convert'}
                    onChange = {handleAmount}
                    />

                    {!baseCurrency || !targetCurrency || !amount  || amount <= 0
                    ?
                    ""
                    :
                    <Button 
                    onClick ={handleConvert}
                    color="info"
                >
                        Convert
                    </Button>
                    }
            </div>
            <div>
                <h1 className="text-center">{convertedAmount}</h1>
            </div>
            <div
                style={{maxHeight: "40vh", overflowY: "auto"}}
            >
                <Rates 
                    rates = {rates}
                />
            </div>
            
        </div>
        );
}
 
export default Forex;
