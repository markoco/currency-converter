import React, { useState } from 'react';
import {FormGroup,Label,Dropdown,DropdownToggle,DropdownMenu,DropdownItem} from 'reactstrap'
import Currencies from './ForexData'

const ForexDropdown = (props) => {

     const [dropdownIsOpen, setDropdownIsopen] = useState(false);
     const [currency, setCurrency] = useState(null);
    return ( 
        <FormGroup>
                <Label>{props.label}</Label>
                <Dropdown
                    isOpen= { dropdownIsOpen }
                    toggle = { () => setDropdownIsopen(!dropdownIsOpen)}
                >
                    <DropdownToggle caret>{!props.currency
                    ?"Choose Currency"
                    :props.currency.currency
                    }</DropdownToggle>
                    <DropdownMenu>
                        {Currencies.map((currency,index)=>(
                            <DropdownItem
                                key = {index}
                                onClick = { ()=> props.onClick(currency) }
                                // onClick = { ()=> this.setState({currency:currency}) }

                            >
                            {currency.currency}
                            </DropdownItem>

                            
                        ))}
                    </DropdownMenu>
                </Dropdown>
            </FormGroup>
    );
}
 
export default ForexDropdown;





