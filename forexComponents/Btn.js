import React from 'react';
import {Button} from 'reactstrap';
const Btn = ({text,handleNumbers}) => {
    return ( 
        <>
            <Button 
            className="py-3 px-4 m-2" 
            style={{width:"70px", height:"70px"}}
            onClick={()=>handleNumbers(text)}
            >
                {text}
            </Button>
        </>
     );
}
 
export default Btn;